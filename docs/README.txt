==Title==
BEES Mittel Sachsen

==Author==
Steffen Winde

==Prefix==
bees

==Shop Version==
5.2.x/4.9.x

==Version==
1.0.0

==Link==
winde-ganzig.de

==Mail==
inserv@winde-ganzig.de

==Description==
BEES Mittel Sachsen Module

==Installation==
Activate the module in administration area.

==Extend==


==Modules==

==Modified original templates==

==Uninstall==
Disable the module in administration area and delete module folder.