<?php
/**
 ttt
 *
 * @category      module
 * @package       mittelsachsen
 * @author        Steffen Winde
 * @link          winde-ganzig.de
 * @copyright (C) inserv,20192019
 */


$sLangName = 'English';

$aLang = array(
    'charset' => 'UTF-8',
    'beesmittelsachsen' => 'BEES Mittel Sachsen',
    'ARTICLE_EXTEND_360PFAD' => '360 Grad Ansicht Pfad',

    );