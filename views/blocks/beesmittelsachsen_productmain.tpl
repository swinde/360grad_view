[{$smarty.block.parent}]
<!-- Extra large modal -->
[{if $oView->getMediaFiles() || $oDetailsProduct->oxarticles__beesmittelsachsen360pfad->value}]
    <a type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#modalYT" title="360 Grad View">
        360 Grad Ansicht <img src="[{$oViewConf->getModuleUrl('beesmittelsachsen','out/src/img/autorotate.png')}]" >
    </a>
[{/if}]
<br>
<!--Modal: Name-->
<div class="modal fade" id="modalYT" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Body-->
            <div class="modal-body mb-0 p-0">
                <div class="embed-responsive embed-responsive-4by3 z-depth-1-half">
                    [{assign var="oConfig" value=$oViewConf->getConfig()}]
                    [{if $oDetailsProduct->oxarticles__beesmittelsachsen360pfad->value}]
                    <iframe id="productFile" src="/out/pictures/ddmedia/[{$oDetailsProduct->oxarticles__beesmittelsachsen360pfad->value}]">[{$oDetailsProduct->oxarticles__beesmittelsachsen360pfad->value}]</iframe>
                    [{/if}]
                </div>
            </div>
            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-outline-primary btn-rounded btn-md ml-4" data-dismiss="modal">Schliessen</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!--Modal: Name-->

