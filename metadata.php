<?php
/**
 ttt
 *
 * @category      module
 * @package       mittelsachsen
 * @author        Steffen Winde
 * @link          winde-ganzig.de
 * @copyright (C) inserv,20192019
 */

/**
 * Metadata version
 */
$sMetadataVersion = '1.1';

/**
 * Module information
 */
$aModule = [
    'id'          => '360Grad_View',
    'title'       => [
        'de' => '. BEES - 360 Grad View',
        'en' => '. BEES - 360 Grad View',
    ],
    'description' => [
        'de' => '360 Grad View',
        'en' => '360 Grad View',
    ],
    'thumbnail'   => 'out/pictures/picture.png',
    'version'     => '1.0.0',
    'author'      => 'Steffen Winde',
    'url'         => 'winde-ganzig.de',
    'email'       => 'inserv@winde-ganzig.de',
    'extend'      => array(
            ),
    'files'       => [
        'beesmittelsachsenmodule' => 'swinde/360Grad_View/core/beesmittelsachsenmodule.php',
    ],
    'templates'   =>[],
    'blocks'      => [
            [
                'template'  => 'article_extend.tpl',
                'block'  => 'admin_article_extend_media',
                'file' => 'views/admin/tpl/beesmittelsachsen_article_extend.tpl',
            ],
            [
                'template'  => 'page/details/inc/productmain.tpl',
                'block'  => 'details_productmain_360grad',
                'file' => 'views/blocks/beesmittelsachsen_productmain.tpl',
            ],
        ],
    'settings'    => [],
    'events'      => [
        'onActivate'   => 'beesMittelSachsenModule::onActivate',
        'onDeactivate' => 'beesMittelSachsenModule::onDeactivate',
    ],
];