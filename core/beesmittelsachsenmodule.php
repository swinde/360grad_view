<?php
/**
 ttt
 *
 * @category      module
 * @package       mittelsachsen
 * @author        Steffen Winde
 * @link          winde-ganzig.de
 * @copyright (C) inserv,20192019
 */

/**
 * Class beesMittelSachsenModule
 * Handles module setup, provides additional tools and module related helpers.
 *
 * @codeCoverageIgnore
 */
class beesMittelSachsenModule extends oxModule
{

    /**
     * Class constructor.
     * Sets current module main data and loads the rest module info.
     */
    function __construct()
    {
        $sModuleId = '360Grad_View';

        $this->setModuleData(
            array(
                 'id'          => $sModuleId,
                 'title'       => 'BEES Mittel Sachsen',
                 'description' => 'BEES Mittel Sachsen Module',
            )
        );

        $this->load($sModuleId);

        oxRegistry::set('beesMittelSachsenModule', $this);
    }


    /**
     * Module activation script.
     */
    public static function onActivate()
    {
        return self::_dbEvent('install.sql', 'Error activating module: ');
    }

    /**
     * Module deactivation script.
     */
    public static function onDeactivate()
    {
        self::_dbEvent('uninstall.sql', 'Error deactivating module: ');
    }

    /**
     * Clean temp folder content.
     *
     * @param string $sClearFolderPath Sub-folder path to delete from. Should be a full, valid path inside temp folder.
     *
     * @return boolean
     */
    public static function clearTmp($sClearFolderPath = '')
    {
        $sFolderPath = self::_getFolderToClear($sClearFolderPath);
        $hDirHandler = opendir($sFolderPath);

        if (!empty($hDirHandler)) {
            while (false !== ($sFileName = readdir($hDirHandler))) {
                $sFilePath = $sFolderPath . DIRECTORY_SEPARATOR . $sFileName;
                self::_clear($sFileName, $sFilePath);
            }

            closedir($hDirHandler);
        }

        return true;
    }

    /**
     * Get translated string by the translation code.
     *
     * @param string  $sCode
     * @param boolean $blUseModulePrefix If True - adds the module translations prefix, if False - not.
     *
     * @return string
     */
    public function translate($sCode, $blUseModulePrefix = true)
    {
        if ($blUseModulePrefix) {
            $sCode = 'BEES_MITTELSACHSEN_' . $sCode;
        }

        return oxRegistry::getLang()->translateString($sCode, oxRegistry::getLang()->getBaseLanguage(), false);
    }

    /**
     * Get CMS snippet content by identified ID.
     *
     * @param string $sIdentifier
     * @param bool   $blNoHtml
     *
     * @return string
     */
    public function getCmsContent($sIdentifier, $blNoHtml = true)
    {
        $sValue = '';

        /** @var oxContent|oxI18n $oContent */
        $oContent = oxNew('oxContent');
        $oContent->loadByIdent(trim((string) $sIdentifier));

        if ($oContent->oxcontents__oxcontent instanceof oxField) {
            $sValue = (string) $oContent->oxcontents__oxcontent->getRawValue();
            $sValue = (empty($blNoHtml) ? $sValue : nl2br(strip_tags($sValue)));
        }

        return $sValue;
    }

    /**
     * Get module setting value.
     *
     * @param string  $sModuleSettingName Module setting parameter name (key).
     * @param boolean $blUseModulePrefix  If True - adds the module settings prefix, if False - not.
     *
     * @return mixed
     */
    public function getSetting($sModuleSettingName, $blUseModulePrefix = true)
    {
        if ($blUseModulePrefix) {
            $sModuleSettingName = 'beesMittelSachsen' . (string) $sModuleSettingName;
        }

        return oxRegistry::getConfig()->getConfigParam((string) $sModuleSettingName);
    }

    /**
     * Get module path.
     *
     * @return string Full path to the module directory.
     */
    public function getPath()
    {
        return oxRegistry::getConfig()->getModulesDir() . 'bees/mittelsachsen/';
    }


    /**
     * Install/uninstall event.
     * Executes SQL queries form a file.
     *
     * @param string $sSqlFile      SQL file located in module docs folder (usually install.sql or uninstall.sql).
     * @param string $sFailureError An error message to show on failure.
     */
    protected static function _dbEvent($sSqlFile, $sFailureError = 'Operation failed: ')
    {
        try {
            $oDb  = oxDb::getDb();
            $sSql = file_get_contents(dirname(__FILE__) . '/../docs/' . (string) $sSqlFile);
            $aSql = (array) explode(';', $sSql);

            foreach ($aSql as $sQuery) {
                if (!empty($sQuery)) {
                    $oDb->execute($sQuery);
                }
            }
        } catch (Exception $ex) {
            error_log($sFailureError . $ex->getMessage());
        }


        self::clearTmp();

        return true;
    }
    
    /**
     * Check if provided path is inside eShop `tpm/` folder or use the `tmp/` folder path.
     *
     * @param string $sClearFolderPath
     *
     * @return string
     */
    protected static function _getFolderToClear($sClearFolderPath = '')
    {
        $sTempFolderPath = (string) oxRegistry::getConfig()->getConfigParam('sCompileDir');

        if (!empty($sClearFolderPath) and (strpos($sClearFolderPath, $sTempFolderPath) !== false)) {
            $sFolderPath = $sClearFolderPath;
        } else {
            $sFolderPath = $sTempFolderPath;
        }

        return $sFolderPath;
    }

    /**
     * Check if resource could be deleted, then delete it's a file or
     * call recursive folder deletion if it's a directory.
     *
     * @param string $sFileName
     * @param string $sFilePath
     */
    protected static function _clear($sFileName, $sFilePath)
    {
        if (!in_array($sFileName, array('.', '..', '.gitkeep', '.htaccess'))) {
            if (is_file($sFilePath)) {
                @unlink($sFilePath);
            } else {
                self::clearTmp($sFilePath);
            }
        }
    }
}